﻿namespace BlackPlain.DI
{
    public enum ResolutionFrequency
    {
        Once,
        OncePerRequest
    }
}