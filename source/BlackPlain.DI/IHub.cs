﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public interface IHub
    {
        void Clear();

        void CreateResolver<TInterface, TInstance>()
            where TInterface : class
            where TInstance : class, TInterface;

        void CreateResolver<TInterface, TInstance>(ResolutionFrequency resolutionFrequency)
            where TInterface : class
            where TInstance : class, TInterface;

        void CreateResolver<TInterface, TInstance>(object key)
            where TInterface : class
            where TInstance : class, TInterface;

        void CreateResolver<TInterface, TInstance>(ResolutionFrequency resolutionFrequency, object key)
            where TInterface : class
            where TInstance : class, TInterface;

        void CreateResolverBundle<TBundle>()
            where TBundle : class, IResolverBundle;

        void RegisterResolverBundle<TBundle>(ResolutionFrequency resolutionFrequency)
            where TBundle : class, IResolverBundle;

        TInstance Resolve<TInstance>() where TInstance : class;

        TInstance Resolve<TInstance>(object key) where TInstance : class;

        object Resolve(Type type);

        object Resolve(Type type, object key);

        IEnumerable<object> ResolveCollection(Type type);

        IEnumerable<object> ResolveCollection(Type type, object key);

        Task<TInstance> ResolveAsync<TInstance>(CancellationToken cancellationToken = default) where TInstance : class;

        Task<TInstance> ResolveAsync<TInstance>(object key, CancellationToken cancellationToken = default) where TInstance : class;

        Task<object> ResolveAsync(Type type, CancellationToken cancellationToken = default);

        Task<object> ResolveAsync(Type type, object key, CancellationToken cancellationToken = default);

        Task<IEnumerable<object>> ResolveCollectionAsync(Type type, CancellationToken cancellationToken = default);

        Task<IEnumerable<object>> ResolveCollectionAsync(Type type, object key, CancellationToken cancellationToken = default);

        void SetResolution<TInterface>(TInterface instance);

        void LoadBundles();
    }
}