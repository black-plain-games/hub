﻿using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public interface IInitialize
    {
        Task InitializeAsync(CancellationToken cancellationToken = default);
    }
}