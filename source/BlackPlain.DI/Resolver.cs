﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public partial class Hub
    {
        internal class Resolver
        {
            public ResolutionFrequency Frequency { get; }

            public object Resolution { get; private set; }

            public Type ResolutionType { get; }

            public bool IsFixedResolution { get; }

            public object Key { get; }

            public Resolver(Type resolutionType, object resolution, object key)
            {
                ResolutionType = resolutionType;

                Resolution = resolution;

                Frequency = ResolutionFrequency.Once;

                IsFixedResolution = true;

                Key = key;
            }

            public Resolver(Type resolutionType, ResolutionFrequency resolutionFrequency, object key)
            {
                ResolutionType = resolutionType;

                Frequency = resolutionFrequency;

                Key = key;
            }

            public void Clear()
            {
                _hasResolved = false;

                Resolution = null;
            }

            [Obsolete("Please use async alternative")]
            public object Resolve(Hub services) => ResolveAsync(services).GetAwaiter().GetResult();

            public async Task<object> ResolveAsync(Hub services, CancellationToken cancellationToken = default)
            {
                cancellationToken.ThrowIfCancellationRequested();

                if (IsFixedResolution || (Frequency == ResolutionFrequency.Once && _hasResolved))
                {
                    Log($"Returning previously defined resolution for type '{ResolutionType}' of instance type '{Resolution?.GetType()}'");

                    return Resolution;
                }

                Log($"Resolving type '{ResolutionType}'");

                var ctor = ResolutionType.GetConstructors().FirstOrDefault();

                if (ctor == null)
                {
                    throw new MissingMethodException($"Type '{ResolutionType}' does not have a public constructor.");
                }

                var parameters = ctor.GetParameters();

                var args = new object[parameters.Length];

                for (var parameterIndex = 0; parameterIndex < parameters.Length; parameterIndex++)
                {
                    var attribute = (KeyedAttribute)parameters[parameterIndex].GetCustomAttributes(typeof(KeyedAttribute), true).FirstOrDefault();

                    var key = attribute?.Key;

                    args[parameterIndex] = await services.ResolveAsync(parameters[parameterIndex].ParameterType, key, cancellationToken);
                }

                var output = Activator.CreateInstance(ResolutionType, args);

                _hasResolved = true;

                if (output is IInitialize initializable)
                {
                    Log($"Initializing instance of type '{ResolutionType}'");

                    await initializable.InitializeAsync(cancellationToken);
                }

                Resolution = output;

                return output;
            }

            private bool _hasResolved;
        }
    }
}