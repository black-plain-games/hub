﻿namespace BlackPlain.DI
{
    public interface IResolverBundle
    {
        void SetResolutions(IHub hub);

        void CreateResolvers(IHub hub);
    }
}