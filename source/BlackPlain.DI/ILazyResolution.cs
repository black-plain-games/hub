﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public interface ILazyResolution<T>
        where T : class
    {
        [Obsolete("Please use async alternative")]
        T Instance { get; }

        Task<T> GetInstanceAsync(CancellationToken cancellationToken = default);
    }
}