﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public partial class Hub : IHub
    {
        public static bool LogEverything { get; set; }

        internal static void Log(string message)
        {
            if (!LogEverything)
            {
                return;
            }

            Debug.WriteLine(message);
        }

        static Hub()
        {
            lock (_resolverLock)
            {
                Log("Creating resolvers dictionary");

                _resolvers = new Dictionary<Type, ICollection<Resolver>>
                {
                    { typeof(IResolverBundle), new List<Resolver>() }
                };
            }
        }

        public Hub()
        {
            Log("Constructing Hub");

            Clear();

            SetResolution<IHub>(this);
        }

        public static void CreateResolverStatic<TInterface, TInstance>()
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolverStatic<TInterface, TInstance>(ResolutionFrequency.Once);
        }

        public static void CreateResolverStatic<TInterface, TInstance>(ResolutionFrequency resolutionFrequency)
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolver(typeof(TInterface), typeof(TInstance), resolutionFrequency, null);
        }

        public static void RegisterResolverBundleStatic<TBundle>()
            where TBundle : class, IResolverBundle
        {
            RegisterResolverBundleStatic<TBundle>(ResolutionFrequency.Once);
        }

        public static void RegisterResolverBundleStatic<TBundle>(ResolutionFrequency resolutionFrequency)
            where TBundle : class, IResolverBundle
        {
            CreateResolverStatic<IResolverBundle, TBundle>(resolutionFrequency);
        }

        public void CreateResolverBundle<TBundle>()
            where TBundle : class, IResolverBundle
        {
            RegisterResolverBundle<TBundle>(ResolutionFrequency.Once);
        }

        public void RegisterResolverBundle<TBundle>(ResolutionFrequency resolutionFrequency)
            where TBundle : class, IResolverBundle
        {
            CreateResolver<IResolverBundle, TBundle>(resolutionFrequency);
        }

        [Obsolete("Please use async alternative")]
        public void LoadBundles() => LoadBundlesAsync().GetAwaiter().GetResult();

        public async Task LoadBundlesAsync(CancellationToken cancellationToken = default)
        {
            Log("Trying to load bundles");

            lock (_resolverLock)
            {
                switch (_loadBundleStatus)
                {
                    case LoadBundleStatus.Loading:
                        Log("Bundles are already loading");
                        return;

                    case LoadBundleStatus.Loaded:
                        Log("Bundles have already been loaded");
                        return;
                }
            }

            _loadBundleStatus = LoadBundleStatus.Loading;

            try
            {
                Log("Loading bundles");

                var loadedBundles = new List<IResolverBundle>();

                var allBundles = await ResolveAsync<IEnumerable<IResolverBundle>>();

                var bundlesToLoad = allBundles.Where(rb => !loadedBundles.Contains(rb));

                while (bundlesToLoad.Any())
                {
                    cancellationToken.ThrowIfCancellationRequested();

                    var bundle = bundlesToLoad.First();

                    Log($"Setting resolutions for bundle '{bundle.GetType()}'");

                    bundle.SetResolutions(this);

                    Log($"Creating resolvers for bundle '{bundle.GetType()}'");

                    bundle.CreateResolvers(this);

                    loadedBundles.Add(bundle);
                }

                _loadBundleStatus = LoadBundleStatus.Loaded;
            }
            catch
            {
                _loadBundleStatus = LoadBundleStatus.NotLoaded;

                throw;
            }
        }

        private enum LoadBundleStatus
        {
            NotLoaded,
            Loading,
            Loaded
        }

        [Obsolete("Please use async alternative")]
        public TInstance Resolve<TInstance>() where TInstance : class => Resolve(typeof(TInstance), null) as TInstance;

        [Obsolete("Please use async alternative")]
        public TInstance Resolve<TInstance>(object key) where TInstance : class => Resolve(typeof(TInstance), key) as TInstance;

        [Obsolete("Please use async alternative")]
        public object Resolve(Type type) => Resolve(type, null);

        [Obsolete("Please use async alternative")]
        public object Resolve(Type type, object key) => ResolveAsync(type, key).GetAwaiter().GetResult();

        [Obsolete("Please use async alternative")]
        public IEnumerable<object> ResolveCollection(Type type) => ResolveCollection(type, null);

        [Obsolete("Please use async alternative")]
        public IEnumerable<object> ResolveCollection(Type type, object key) => ResolveCollectionAsync(type, key).GetAwaiter().GetResult();

        public async Task<TInstance> ResolveAsync<TInstance>(CancellationToken cancellationToken = default) where TInstance : class => await ResolveAsync(typeof(TInstance), null, cancellationToken) as TInstance;

        public async Task<TInstance> ResolveAsync<TInstance>(object key, CancellationToken cancellationToken = default) where TInstance : class => await ResolveAsync(typeof(TInstance), key, cancellationToken) as TInstance;

        public async Task<object> ResolveAsync(Type type, CancellationToken cancellationToken = default) => await ResolveAsync(type, null, cancellationToken);

        public async Task<object> ResolveAsync(Type type, object key, CancellationToken cancellationToken = default)
        {
            await LoadBundlesAsync(cancellationToken);

            var resolver = GetResolver(type, key);

            if (resolver == null)
            {
                Log($"No resolver found for type '{type}'. Attempting to resolve it as if it were an IEnumerable.");

                var innerType = GetEnumerableInnerType(type);

                if (innerType == null)
                {
                    throw new KeyNotFoundException($"No resolvers registered for type '{type.Name}'.");
                }

                var innerResolvers = GetResolvers(innerType, key);

                if (innerResolvers == null)
                {
                    throw new KeyNotFoundException($"No resolvers registered for type '{type.Name}'.");
                }

                var results = new List<object>();

                foreach (var innerResolver in innerResolvers)
                {
                    results.Add(await innerResolver.ResolveAsync(this, cancellationToken));
                }

                var typedResults = Cast(results, innerType);

                return typedResults;
            }

            return await resolver.ResolveAsync(this, cancellationToken);
        }
        
        public async Task<IEnumerable<object>> ResolveCollectionAsync(Type type, CancellationToken cancellationToken = default) => await ResolveCollectionAsync(type, null, cancellationToken);

        public async Task<IEnumerable<object>> ResolveCollectionAsync(Type type, object key, CancellationToken cancellationToken = default)
        {
            await LoadBundlesAsync(cancellationToken);

            var resolvers = GetResolvers(type, key);

            if (resolvers?.Any() != true)
            {
                return Enumerable.Empty<object>();
            }

            var output = new List<object>();

            foreach (var resolver in resolvers)
            {
                output.Add(await resolver.ResolveAsync(this, cancellationToken));
            }

            return output;
        }

        public void CreateResolver<TInterface, TInstance>()
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolver<TInterface, TInstance>(ResolutionFrequency.Once);
        }

        public void CreateResolver<TInterface, TInstance>(ResolutionFrequency resolutionFrequency)
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolver(typeof(TInterface), typeof(TInstance), resolutionFrequency, null);
        }

        public void CreateResolver<TInterface, TInstance>(object key)
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolver(typeof(TInterface), typeof(TInstance), ResolutionFrequency.Once, key);
        }

        public void CreateResolver<TInterface, TInstance>(ResolutionFrequency resolutionFrequency, object key)
            where TInterface : class
            where TInstance : class, TInterface
        {
            CreateResolver(typeof(TInterface), typeof(TInstance), resolutionFrequency, key);
        }

        public void Clear()
        {
            Log("Clearing resolvers");

            lock (_resolverLock)
            {
                foreach (var resolverGroup in _resolvers)
                {
                    Log($"Clearing resolver group for type '{resolverGroup.Key}'");

                    var fixedResolvers = resolverGroup.Value.Where(r => r.IsFixedResolution).ToArray();

                    Log($"Removing {fixedResolvers.Count()} fixed resolvers");

                    foreach (var resolver in fixedResolvers)
                    {
                        Log($"Removing '{resolver.Resolution?.GetType()}' resolution");

                        resolverGroup.Value.Remove(resolver);
                    }

                    foreach (var resolver in resolverGroup.Value)
                    {
                        Log($"Clearing resolver '{resolver.ResolutionType}'");

                        resolver.Clear();
                    }
                }

                _loadBundleStatus = LoadBundleStatus.NotLoaded;
            }
        }

        public void SetResolution<TInterface>(TInterface instance)
        {
            SetResolution(instance, null);
        }

        public void SetResolution<TInterface>(TInterface instance, object key)
        {
            var interfaceType = typeof(TInterface);

            Log($"Settings resolution for type '{interfaceType}' to instance of type '{instance?.GetType()}'");

            AddResolver(interfaceType, new Resolver(instance.GetType(), instance, key));
        }

        public override string ToString()
        {
            return _identifier.ToString();
        }

        private static void CreateResolver(Type interfaceType, Type instanceType, ResolutionFrequency resolutionFrequency, object key)
        {
            Log($"Attempting to create resolver for interface type '{interfaceType}' of instance type '{instanceType}' with frequency '{resolutionFrequency}'");

            if (interfaceType == null)
            {
                throw new ArgumentNullException(nameof(interfaceType));
            }

            if (instanceType == null)
            {
                throw new ArgumentNullException(nameof(instanceType));
            }

            if (!interfaceType.IsAssignableFrom(instanceType))
            {
                throw new InvalidOperationException($"Resolution type '{instanceType.Name}' must be subclass of interface type '{interfaceType.Name}'.");
            }

            AddResolver(interfaceType, new Resolver(instanceType, resolutionFrequency, key));

            Log($"Attempting to create lazy resolver for interface type '{interfaceType}' of instance type '{instanceType}' with frequency '{resolutionFrequency}'");

            var lazyResolutionInterfaceGenericType = typeof(ILazyResolution<>);

            var lazyResolutionInterfaceType = lazyResolutionInterfaceGenericType.MakeGenericType(interfaceType);

            var lazyResolutionGenericType = typeof(LazyResolution<>);

            var lazyResolutionType = lazyResolutionGenericType.MakeGenericType(interfaceType);

            AddResolver(lazyResolutionInterfaceType, new Resolver(lazyResolutionType, resolutionFrequency, key));
        }

        private static void AddResolver(Type interfaceType, Resolver resolver)
        {
            if (!_resolvers.ContainsKey(interfaceType))
            {
                Log($"Adding first resolver of type '{interfaceType}'");

                _resolvers.Add(interfaceType, new List<Resolver>
                {
                    resolver
                });
            }
            else if (!_resolvers[interfaceType].Any(r => AreEqual(r, resolver)))
            {
                Log($"Adding resolver of type '{interfaceType}'");

                _resolvers[interfaceType].Add(resolver);
            }
        }

        private static bool AreEqual(Resolver source, Resolver target)
        {
            if (source.IsFixedResolution)
            {
                if (target.IsFixedResolution)
                {
                    return source.Resolution == target.Resolution;
                }

                return false;
            }

            return source.ResolutionType == target.ResolutionType &&
                ((source.Key == null && target.Key == null) ||
                 (source.Key != null && source.Key.Equals(target.Key)) ||
                 (target.Key != null && target.Key.Equals(source.Key)));
        }

        private static Resolver GetResolver(Type type, object key)
        {
            var resolvers = GetResolvers(type, key);

            var output = resolvers?.FirstOrDefault();

            if (output != null)
            {
                Log($"Resolver found for type '{type}', Key: '{key}'. IsFixed: {output.IsFixedResolution}, ResolutionType: '{output.ResolutionType}'");
            }

            return output;
        }

        private static IEnumerable<Resolver> GetResolvers(Type type, object key)
        {
            IEnumerable<Resolver> output = null;

            if (_resolvers.ContainsKey(type))
            {
                output = _resolvers[type];

                if (key != null)
                {
                    output = output.Where(r => key.Equals(r.Key));
                }
            }

            if (output == null)
            {
                Log($"No resolvers found for type '{type}', Key: '{key}'");
            }
            else
            {
                Log($"{output.Count()} resolvers found for type '{type}', Key: '{key}'");
            }

            return output;
        }

        private static Type GetEnumerableInnerType(Type type)
        {
            if (!type.IsGenericType)
            {
                return null;
            }

            var genericTypeDefinition = type.GetGenericTypeDefinition();

            if (genericTypeDefinition != typeof(IEnumerable<>))
            {
                return null;
            }

            return type.GetGenericArguments().FirstOrDefault();
        }

        private static object Cast(IEnumerable<object> source, Type targetType)
        {
            var castMethodDefinition = typeof(Enumerable).GetMethod("Cast");

            var castMethod = castMethodDefinition.MakeGenericMethod(targetType);

            return castMethod.Invoke(null, new[] { source });
        }

        private static readonly object _resolverLock = new object();

        private static readonly IDictionary<Type, ICollection<Resolver>> _resolvers;

        private readonly Guid _identifier = Guid.NewGuid();

        private static LoadBundleStatus _loadBundleStatus;
    }
}