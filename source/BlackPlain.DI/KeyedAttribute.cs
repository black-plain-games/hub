﻿using System;

namespace BlackPlain.DI
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public class KeyedAttribute : Attribute
    {
        public object Key { get; }

        public KeyedAttribute(object key)
        {
            Key = key;
        }
    }
}