﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlackPlain.DI
{
    public class LazyResolution<T> : ILazyResolution<T>
        where T : class
    {
        [Obsolete("Please use async alternative")]
        public T Instance => _hub.Resolve<T>();

        public async Task<T> GetInstanceAsync(CancellationToken cancellationToken = default) => await _hub.ResolveAsync<T>(cancellationToken);

        public LazyResolution(IHub hub)
        {
            _hub = hub;
        }

        private readonly IHub _hub;
    }
}