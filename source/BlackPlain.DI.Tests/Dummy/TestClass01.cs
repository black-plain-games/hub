﻿namespace BlackPlain.DI.Tests.Dummy
{
    public class TestClass01 : ITestInterface01
    {
        public static int CreatedCount { get; set; }

        public TestClass01()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass01_01 : ITestInterface01
    {
        public static int CreatedCount { get; set; }

        public TestClass01_01()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass02 : ITestInterface02
    {
        public static int CreatedCount { get; set; }

        public TestClass02()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass03 : ITestInterface03
    {
        public static int CreatedCount { get; set; }

        public TestClass03()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass04 : ITestInterface04
    {
        public static int CreatedCount { get; set; }

        public TestClass04()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass05 : ITestInterface05
    {
        public static int CreatedCount { get; set; }

        public TestClass05()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass06 : ITestInterface06
    {
        public static int CreatedCount { get; set; }

        public TestClass06()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass07 : ITestInterface07
    {
        public static int CreatedCount { get; set; }

        public TestClass07()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass08 : ITestInterface08
    {
        public static int CreatedCount { get; set; }

        public TestClass08()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }

    public class TestClass09 : ITestInterface09
    {
        public static int CreatedCount { get; set; }

        public TestClass09()
        {
            CreatedCount++;
        }

        public int Count { get; set; }
    }
}