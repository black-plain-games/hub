﻿namespace BlackPlain.DI.Tests.Dummy
{
    public class TestBundle01 : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<ITestInterface01, TestClass01>();
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}