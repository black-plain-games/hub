﻿namespace BlackPlain.DI.Tests.Dummy
{
    public interface ITestClassWithParameters
    {
        ITestInterface02 A { get; }
    }

    public class TestClassWithParameters : ITestClassWithParameters
    {
        public ITestInterface02 A { get; }

        public TestClassWithParameters(ITestInterface02 a)
        {
            A = a;
        }
    }
}