﻿namespace BlackPlain.DI.Tests.Dummy
{
    public interface ITestClassWithKeys
    {
        ITestInterface01 A { get; }

        ITestInterface01 B { get; }
    }

    public class TestClassWithKeys : ITestClassWithKeys
    {
        public ITestInterface01 A { get; }

        public ITestInterface01 B { get; }

        public TestClassWithKeys([Keyed(ClassTag.TypeA)]ITestInterface01 a, [Keyed(ClassTag.TypeB)]ITestInterface01 b)
        {
            A = a;

            B = b;
        }
    }
}