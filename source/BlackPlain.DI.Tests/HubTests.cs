﻿using BlackPlain.DI.Tests.Dummy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace BlackPlain.DI.Tests
{
    [TestClass]
    public class HubTests
    {
        protected IHub _services;

        [TestInitialize]
        public void Initialize()
        {
            _services = new Hub();

            TestClass01.CreatedCount = 0;
        }

        #region Tests

        [TestMethod]
        public void StaticResolver2Adds1RemoveCreates1Resolver()
        {
            _services.SetResolution<ITestInterface01>(new TestClass01());

            _services.Clear();

            _services.SetResolution<ITestInterface01>(new TestClass01());
        }

        [TestMethod]
        public void Hub_CreatResolvers_NoException()
        {
            _services.CreateResolver<ITestInterface02, TestClass02>();
        }

        [TestMethod]
        public void Hub_Resolve_LocalAssembly()
        {
            _services.CreateResolver<ITestInterface03, TestClass03>();

            var obj1 = _services.Resolve<ITestInterface03>();

            Assert.IsNotNull(obj1);

            Assert.IsTrue(obj1 is TestClass03);
        }

        [TestMethod]
        public void Hub_Resolve_ResolutionFrequency_Default_Is_Once()
        {
            _services.CreateResolver<ITestInterface04, TestClass04>(ResolutionFrequency.Once);

            var a = _services.Resolve<ITestInterface04>();

            var b = _services.Resolve<ITestInterface04>();

            Assert.AreSame(a, b);
        }

        [TestMethod]
        public void Hub_Resolve_ResolutionFrequency_Once_Succeeds()
        {
            _services.CreateResolver<ITestInterface05, TestClass05>(ResolutionFrequency.Once);

            var a = _services.Resolve<ITestInterface05>();

            var b = _services.Resolve<ITestInterface05>();

            Assert.AreSame(a, b);
        }

        [TestMethod]
        public void Hub_Resolve_ResolutionFrequency_OncePerRequest_Succeeds()
        {
            _services.CreateResolver<ITestInterface06, TestClass06>(ResolutionFrequency.OncePerRequest);

            var a = _services.Resolve<ITestInterface06>();

            var b = _services.Resolve<ITestInterface06>();

            Assert.AreNotSame(a, b);
        }

        [TestMethod]
        public void Hub_Resolve_LazyResolution_Is_Lazy()
        {
            _services.CreateResolver<ITestInterface07, TestClass07>(ResolutionFrequency.Once);

            Assert.AreEqual(0, TestClass07.CreatedCount);

            var lazyA = _services.Resolve<ILazyResolution<ITestInterface07>>();

            Assert.AreEqual(0, TestClass07.CreatedCount);

            var a = lazyA.Instance;

            Assert.AreEqual(1, TestClass07.CreatedCount);
        }

        [TestMethod]
        public void Hub_Resolve_LazyResolution_ResolutionFrequency_Once_Succeeds()
        {
            _services.CreateResolver<ITestInterface08, TestClass08>(ResolutionFrequency.Once);

            Assert.AreEqual(0, TestClass08.CreatedCount);

            var lazyA = _services.Resolve<ILazyResolution<ITestInterface08>>();

            Assert.AreEqual(0, TestClass08.CreatedCount);

            var a = lazyA.Instance;

            Assert.AreEqual(1, TestClass08.CreatedCount);

            var lazyB = _services.Resolve<ILazyResolution<ITestInterface08>>();

            Assert.AreEqual(1, TestClass08.CreatedCount);

            var b = lazyB.Instance;

            Assert.AreEqual(1, TestClass08.CreatedCount);
        }

        [TestMethod]
        public void Hub_Resolve_LazyResolution_ResolutionFrequency_OncePerRequest_Succeeds()
        {
            _services.CreateResolver<ITestInterface09, TestClass09>(ResolutionFrequency.OncePerRequest);

            Assert.AreEqual(0, TestClass09.CreatedCount);

            var lazyA = _services.Resolve<ILazyResolution<ITestInterface09>>();

            Assert.AreEqual(0, TestClass09.CreatedCount);

            var a = lazyA.Instance;

            Assert.AreEqual(1, TestClass09.CreatedCount);

            var lazyB = _services.Resolve<ILazyResolution<ITestInterface09>>();

            Assert.AreEqual(1, TestClass09.CreatedCount);

            var b = lazyB.Instance;

            Assert.AreEqual(2, TestClass09.CreatedCount);
        }

        [TestMethod]
        public void Hub_CreateResolverBundle_creates_resolvers()
        {
            object a = new List<IResolverBundle>();

            var b = a as IEnumerable<IResolverBundle>;

            _services.CreateResolverBundle<TestBundle01>();

            var result = _services.Resolve<ITestInterface01>();

            Assert.IsNotNull(result);

            Assert.IsTrue(result is TestClass01);
        }

        [TestMethod]
        public void Hub_Resolve_with_key_works()
        {
            _services.CreateResolver<ITestInterface01, TestClass01>(ClassTag.TypeA);

            _services.CreateResolver<ITestInterface01, TestClass01_01>(ClassTag.TypeB);

            var a = _services.Resolve<ITestInterface01>(ClassTag.TypeA);

            Assert.IsInstanceOfType(a, typeof(TestClass01));

            var b = _services.Resolve<ITestInterface01>(ClassTag.TypeB);

            Assert.IsInstanceOfType(b, typeof(TestClass01_01));
        }

        [TestMethod]
        public void Hub_Resolve_Keyed_attribute_works()
        {
            _services.CreateResolver<ITestInterface01, TestClass01>(ClassTag.TypeA);

            _services.CreateResolver<ITestInterface01, TestClass01_01>(ClassTag.TypeB);

            _services.CreateResolver<ITestClassWithKeys, TestClassWithKeys>();

            var x = _services.Resolve<ITestClassWithKeys>();

            Assert.IsInstanceOfType(x.A, typeof(TestClass01));

            Assert.IsInstanceOfType(x.B, typeof(TestClass01_01));
        }

        [TestMethod]
        public void Hub_Resolve_parameters_without_Keyed_work()
        {
            _services.CreateResolver<ITestInterface02, TestClass02>();

            _services.CreateResolver<ITestClassWithParameters, TestClassWithParameters>();

            var x = _services.Resolve<ITestClassWithParameters>();

            Assert.IsNotNull(x.A);

            Assert.IsInstanceOfType(x.A, typeof(TestClass02));
        }

        #endregion
    }
}